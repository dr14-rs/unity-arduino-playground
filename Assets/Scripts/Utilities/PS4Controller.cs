using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PS4Controller : MonoBehaviour
{
    public GameObject control;
    Rigidbody rb;

    public float leftStickX = 0;
    public float leftStickY = 0;
    public float rightStickX = 0;
    public float rightStickY = 0;

    private Gamepad controller = null;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();

        for (int i = 0; i < Gamepad.all.Count; i++)
        {
             Debug.Log(Gamepad.all[i].name);
            if (Gamepad.all[i].name == "DualShock4GamepadHID") {
                Debug.Log("Controller found");
                controller = Gamepad.all[i];
                break;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (controller!= null) {

            leftStickX = controller.leftStick.right.value - controller.leftStick.left.value;
            leftStickY = controller.leftStick.up.value - controller.leftStick.down.value;
            
            leftStickX = controller.rightStick.right.value - controller.rightStick.left.value;
            leftStickY = controller.rightStick.up.value - controller.rightStick.down.value;

        }


    }
}
