
using System;

public class BitWiser {
    private int index = 0;
    private String name;
    public BitWiser(int index,  String name) {
        this.index = index;
        this.name = name;

    }
    private bool bitRead(uint value ) {
        return ((value >> index) & 0x01) == 1;
    }

    private uint bitSet(uint value ) {
        return value |= (uint)(1 << (index));
    }

    private uint bitClear(uint value) {
        return ((value) &= (uint)~(1 << (index)));
    }

    public uint  switchOn(uint value) {
        if (!bitRead(value)) {   
            return  bitSet(value);    
        }
        return value;
    }

    public uint switchOff(uint value) {
        if (bitRead(value)) {   
            return bitClear(value); 
        }
        return value;
    }
    public bool status(uint value) {
        return bitRead(value);
    }  
}