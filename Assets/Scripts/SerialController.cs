/**
 * Ardity (Serial Communication for Arduino + Unity)
 * Author: Daniel Wilches <dwilches@gmail.com>
 *
 * This work is released under the Creative Commons Attributions license.
 * https://creativecommons.org/licenses/by/2.0/
 */
using System;

using UnityEngine;
using System.Threading;
using System.Collections.Generic;

/**
 * This class allows a Unity program to continually check for messages from a
 * serial device.
 *
 * It creates a Thread that communicates with the serial port and continually
 * polls the messages on the wire.
 * That Thread puts all the messages inside a Queue, and this SerialController
 * class polls that queue by means of invoking SerialThread.GetSerialMessage().
 *
 * The serial device must send its messages separated by a newline character.
 * Neither the SerialController nor the SerialThread perform any validation
 * on the integrity of the message. It's up to the one that makes sense of the
 * data.
 */


public struct SDE_to_PC
{
    public uint buttonState;
    public uint displayed_speed;
    public uint displayed_rmp;
    public uint acceleratorValue;
    public uint brakeValue;
    public uint clutchValue;
};

public struct SDE_to_ARD
{
    public uint speed;
    public uint rmp;
    public uint indicators;
    public uint engine_state;
};


public enum Indicators
{
    Immobilizer = 0,
    ABS = 1,
    CheckEngine = 2,
    Airbag = 3,
    Oil = 4,
    Battery = 5,
    FogLight = 6,
    LowLight = 7,
    HighLight = 8,
    LeftTurn = 9,
    RightTurn = 10,
    WaterPump = 11,
    TyrePressure = 12,
    Doors = 13,
    Seatbelt = 14,
    Handbrake = 15,
    FogLightBack = 17,
    SomeGear = 18,
    SteeringWheel = 19,
    Refuel = 20,
    Attention = 21
}


public enum Switches
{
    WIPERS_WATER_FRONT = 0,
    WIPERS_3 = 1,
    WIPERS_2 = 2,
    WIPERS_1 = 3,
    WIPERS_BACK = 4,
    WIPERS_WATER_BACK = 5,
    LEFT_BL = 6,
    RIGHT_BL = 7,
    FOG_LIGHT = 8,
    NOTHING = 9,
    BEAM_LIGHT = 10,
    PARK_LIGHT = 11,
    LOW_LIGHT = 12,
    NOTHING_2 = 13,
    IGNITION = 14,
    STARTER = 15,
    HAND_BRAKE = 16,
};

public class SerialController : MonoBehaviour
{
    [Tooltip("Port name with which the SerialPort object will be created.")]
    public string portName = "COM3";

    [Tooltip("Baud rate that the serial device is using to transmit data.")]
    public int baudRate = 115200;

    [Tooltip("After an error in the serial communication, or an unsuccessful " +
             "connect, how many milliseconds we should wait.")]
    public int reconnectionDelay = 1000;

    [Tooltip("Maximum number of unread data messages in the queue. " +
             "New or old (depending on \"Drop Old Message\" configuration) messages will be discarded.")]
    public int maxUnreadMessages = 1;

    [Tooltip("When the queue is full, prefer dropping the oldest message in the queue " +
             "instead of the new incoming message. Use this if you prefer to keep the " +
             "newest messages from the port.")]
    public bool dropOldMessage;


    // Constants used to mark the start and end of a connection. There is no
    // way you can generate clashing messages from your serial device, as I
    // compare the references of these strings, no their contents. So if you
    // send these same strings from the serial device, upon reconstruction they
    // will have different reference ids.
    public const string SERIAL_DEVICE_CONNECTED = "__Connected__";
    public const string SERIAL_DEVICE_DISCONNECTED = "__Disconnected__";

    // Internal reference to the Thread and the object that runs in it.
    protected Thread thread;
    protected SerialThreadLines<SDE_to_ARD, SDE_to_PC> serialThread;


    private SDE_to_ARD toArduino = new SDE_to_ARD();
    private SDE_to_PC fromArduino = new SDE_to_PC();

    private bool connected = false;
    private bool messageProceeded = true;
    private Dictionary<Indicators, BitWiser> indicators = new Dictionary<Indicators, BitWiser>(){
        {Indicators.Immobilizer, new BitWiser(0,"Immobilizer")},
        {Indicators.ABS, new BitWiser(1, "ABS")},
        {Indicators.CheckEngine, new BitWiser(2, "CheckEngine")},
        {Indicators.Airbag, new BitWiser(3, "Airbag")},
        {Indicators.Oil, new BitWiser(4, "Oil")},
        {Indicators.Battery, new BitWiser(5, "Battery")},
        {Indicators.FogLight, new BitWiser(6, "FogLight")},
        {Indicators.LowLight, new BitWiser(7, "LowLight")},
        {Indicators.HighLight, new BitWiser(8, "HighLight")},
        {Indicators.LeftTurn, new BitWiser(9, "LeftTurn")},
        {Indicators.RightTurn, new BitWiser(10, "RightTurn")},
        {Indicators.WaterPump, new BitWiser(11, "WaterPump")},
        {Indicators.TyrePressure, new BitWiser(12, "TyrePressure")},
        {Indicators.Doors, new BitWiser(13, "Doors")},
        {Indicators.Seatbelt, new BitWiser(14, "Seatbelt")},
        {Indicators.Handbrake, new BitWiser(15, "Handbrake")},
        {Indicators.FogLightBack, new BitWiser(17, "FogLightBack")},
        {Indicators.SomeGear, new BitWiser(18, "SomeGear")},
        {Indicators.SteeringWheel, new BitWiser(19, "SteeringWheel ")},
        {Indicators.Refuel, new BitWiser(20, "Refuel")},
        {Indicators.Attention, new BitWiser(21, "Attention")}
    };
    public Dictionary<Switches, BitWiser> switches = new Dictionary<Switches, BitWiser>(){
        {Switches.WIPERS_WATER_FRONT,  new BitWiser(0, "WIPERS_WATER_FRONT")},
        {Switches.WIPERS_3,  new BitWiser(1, "WIPERS_3")},
        {Switches.WIPERS_2,  new BitWiser(2, "WIPERS_2")},
        {Switches.WIPERS_1,  new BitWiser(3, "WIPERS_1")},
        {Switches.WIPERS_BACK,  new BitWiser(4, "WIPERS_BACK")},
        {Switches.WIPERS_WATER_BACK,  new BitWiser(5, "WIPERS_WATER_BACK")},
        {Switches.LEFT_BL,  new BitWiser(6, "LEFT_BL")},
        {Switches.RIGHT_BL,  new BitWiser(7, "RIGHT_BL")},
        {Switches.FOG_LIGHT,  new BitWiser(8, "FOG_LIGHT")},
        {Switches.NOTHING,  new BitWiser(9, "NOTHING")},
        {Switches.BEAM_LIGHT,  new BitWiser(10, "BEAM_LIGHT")},
        {Switches.PARK_LIGHT,  new BitWiser(11, "PARK_LIGHT")},
        {Switches.LOW_LIGHT,  new BitWiser(12, "LOW_LIGHT")},
        {Switches.NOTHING_2,  new BitWiser(13, "NOTHING_2")},
        {Switches.IGNITION,  new BitWiser(14, "IGNITION")},
        {Switches.STARTER,  new BitWiser(15, "STARTER")},
        {Switches.HAND_BRAKE,  new BitWiser(16, "HAND_BRAKE")},
    };


    void Start()
    {
        toArduino.engine_state = 0;
        toArduino.indicators = 0;
        toArduino.rmp = 0;
        toArduino.speed = 0;

    }

    public void setIndicator(Indicators indicator, bool status) {
        if (status) {
            this.indicators[indicator].switchOn(toArduino.indicators);
        } else {
            this.indicators[indicator].switchOff(toArduino.indicators);
        }
    }

    public void setSpeed(uint speed) {
        toArduino.speed = speed;
    } 

    public void setRmp(uint rmp) {
        toArduino.rmp = rmp;
    } 

    public void setEngineState(uint state) {
        toArduino.engine_state = state;
    } 

    public bool getIndicatorState(Indicators indicator) {
        return this.indicators[indicator].status(toArduino.indicators);
    }

    public bool getButtonState(Switches swtch) {
        return this.switches[swtch].status(fromArduino.buttonState);
    }

    // ------------------------------------------------------------------------
    // Invoked whenever the SerialController gameobject is activated.
    // It creates a new thread that tries to connect to the serial device
    // and start reading from it.
    // ------------------------------------------------------------------------
    void OnEnable()
    {
        serialThread = new SerialThreadLines<SDE_to_ARD, SDE_to_PC>(portName,
                                             baudRate,
                                             reconnectionDelay,
                                             maxUnreadMessages,
                                             dropOldMessage);
        thread = new Thread(new ThreadStart(serialThread.RunForever));
        thread.Start();
    }

    // ------------------------------------------------------------------------
    // Invoked whenever the SerialController gameobject is deactivated.
    // It stops and destroys the thread that was reading from the serial device.
    // ------------------------------------------------------------------------
    void OnDisable()
    {
        // If there is a user-defined tear-down function, execute it before
        // closing the underlying COM port.
        if (userDefinedTearDownFunction != null)
            userDefinedTearDownFunction();

        // The serialThread reference should never be null at this point,
        // unless an Exception happened in the OnEnable(), in which case I've
        // no idea what face Unity will make.
        if (serialThread != null)
        {
            serialThread.RequestStop();
            serialThread = null;
        }

        // This reference shouldn't be null at this point anyway.
        if (thread != null)
        {
            thread.Join();
            thread = null;
        }
    }

    // ------------------------------------------------------------------------
    // Polls messages from the queue that the SerialThread object keeps. Once a
    // message has been polled it is removed from the queue. There are some
    // special messages that mark the start/end of the communication with the
    // device.
    // ------------------------------------------------------------------------
    void Update()
    {

        //---------------------------------------------------------------------
        // Receive data
        //---------------------------------------------------------------------
        if (connected && messageProceeded)
        {
            toArduino.speed = 1234567;
            toArduino.rmp = 100;
            serialThread.SendMessage(toArduino);
            messageProceeded = false;
        }

        // Read the next message from the queue
        var message = serialThread.ReadMessage();
        if (message == null)
            return;
        if (message is string)
        {
            if (ReferenceEquals(message, SerialController.SERIAL_DEVICE_CONNECTED))
            {
                Debug.Log("Connection established");
                connected = true;
            }
            else if (ReferenceEquals(message, SerialController.SERIAL_DEVICE_DISCONNECTED))
            {
                Debug.Log("Connection attempt failed or disconnection detected");
                connected = false;
            }
        }
        else
        {
            fromArduino = (SDE_to_PC)message;
            Debug.Log(String.Format("Speed: {0}, Accelerator: {1} Button State: {2} RPM: {3} ", fromArduino.displayed_speed, fromArduino.acceleratorValue, fromArduino.buttonState, fromArduino.displayed_rmp));
            messageProceeded = true;
        }


       
    }


    // ------------------------------------------------------------------------
    // Executes a user-defined function before Unity closes the COM port, so
    // the user can send some tear-down message to the hardware reliably.
    // ------------------------------------------------------------------------
    public delegate void TearDownFunction();

    private TearDownFunction userDefinedTearDownFunction;

    public void SetTearDownFunction(TearDownFunction userFunction)
    {
        this.userDefinedTearDownFunction = userFunction;
    }
}