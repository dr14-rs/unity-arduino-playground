/**
 * Ardity (Serial Communication for Arduino + Unity)
 * Author: Daniel Wilches <dwilches@gmail.com>
 *
 * This work is released under the Creative Commons Attributions license.
 * https://creativecommons.org/licenses/by/2.0/
 */

using UnityEngine;

using System.IO.Ports;
using System.Runtime.InteropServices;
using System;
using System.Threading;

/**
 * This class contains methods that must be run from inside a thread and others
 * that must be invoked from Unity. Both types of methods are clearly marked in
 * the code, although you, the final user of this library, don't need to even
 * open this file unless you are introducing incompatibilities for upcoming
 * versions.
 * 
 * For method comments, refer to the base class.
 */
public class SerialThreadLines<T,R> : AbstractSerialThread<T,R>
    where T : struct
    where R: struct
{
    public SerialThreadLines(string portName,
                             int baudRate,
                             int delayBeforeReconnecting,
                             int maxUnreadMessages,
                             bool dropOldMessage)
        : base(portName, baudRate, delayBeforeReconnecting, maxUnreadMessages, true, dropOldMessage)
    {
    }

    protected override void SendToWire(object message, SerialPort serialPort)
    {
        serialPort.WriteLine((string) message);
    }

    protected override object ReadFromWire(SerialPort serialPort)
    {
        return  serialPort.ReadLine();
    }

    protected override void SendStructToWire(T message,    SerialPort serialPort) {
        int size = Marshal.SizeOf(message);
        byte[] arr = new byte[size];

        IntPtr ptr = IntPtr.Zero;
        try
        {
            ptr = Marshal.AllocHGlobal(size);
            Marshal.StructureToPtr(message, ptr, true);
            Marshal.Copy(ptr, arr, 0, size);
        }
        finally
        {
            Marshal.FreeHGlobal(ptr);
        }
        serialPort.Write(arr, 0, size);
        while (serialPort.BytesToWrite != 0 ) {
            Thread.Sleep(1);
        }
    }

    // ------------------------------------------------------------------------
    // Reads and returns a message from the serial port.
    // ------------------------------------------------------------------------
    protected override R ReadStructFromWire( SerialPort serialPort) {
        R output = new R();
        int size = Marshal.SizeOf(output);
        int attempt = 0;
        if (serialPort.BytesToRead != size ) {
            Thread.Sleep(5);
            attempt ++;
            if (attempt > 1000) {
                throw new Exception(String.Format("Waited to log for {0} but got only {}", size,serialPort.BytesToRead ));
            }
        }
        byte[] arr = new byte[size];
        serialPort.Read(arr,0, size);
        IntPtr ptr = IntPtr.Zero;
        try
        {
            ptr = Marshal.AllocHGlobal(size);

            Marshal.Copy(arr, 0, ptr, size);

            output = (R)Marshal.PtrToStructure(ptr, output.GetType());
        }
        finally
        {
            Marshal.FreeHGlobal(ptr);
        }
        return output;
    }
}