using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;
using System;


struct SDE_to_PC_TEST
{
    public ulong buttonState;
    public ulong displayed_speed;
    public ulong displayed_rmp;
    public ulong acceleratorValue;
    public ulong brakeValue;
    public ulong clutchValue;
};





public class Test : MonoBehaviour
{
    public GameObject control;
    Rigidbody rb;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Destroy(gameObject, 3f);

        }
    }

    void OnMouseDown()
    {

    }
    private void OnCollisionEnter(Collision collision)
    {
        Debug.Log(collision);
        rb.AddForce(Vector3.up * 300);
    }

    byte[] getBytes(SDE_to_PC str)
    {
        int size = Marshal.SizeOf(str);
        byte[] arr = new byte[size];

        IntPtr ptr = IntPtr.Zero;
        try
        {
            ptr = Marshal.AllocHGlobal(size);
            Marshal.StructureToPtr(str, ptr, true);
            Marshal.Copy(ptr, arr, 0, size);
        }
        finally
        {
            Marshal.FreeHGlobal(ptr);
        }
        return arr;
    }

    SDE_to_PC fromBytes(byte[] arr)
    {
        SDE_to_PC str = new SDE_to_PC();

        int size = Marshal.SizeOf(str);
        IntPtr ptr = IntPtr.Zero;
        try
        {
            ptr = Marshal.AllocHGlobal(size);

            Marshal.Copy(arr, 0, ptr, size);

            str = (SDE_to_PC)Marshal.PtrToStructure(ptr, str.GetType());
        }
        finally
        {
            Marshal.FreeHGlobal(ptr);
        }
        return str;
    }
}

