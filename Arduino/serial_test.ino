#define DEBUG ;
const unsigned long ULONG_MAX = 0UL - 1UL;

struct SDE_to_PC {
  unsigned long buttonState;
  unsigned long displayed_speed;
  unsigned long displayed_rmp;
  unsigned long acceleratorValue;
  unsigned long brakeValue;
  unsigned long clutchValue;
};


struct SDE_to_ARD {
  unsigned long speed;
  unsigned long rmp;
  unsigned long indicators;
  unsigned long engine_state;
};


SDE_to_ARD * data_in  = new SDE_to_ARD;
SDE_to_PC * data_out = new SDE_to_PC;


int msg_size_to_pc  = sizeof(SDE_to_PC);
int msg_size_to_ard = sizeof(SDE_to_ARD);

char * buffIn = new char[msg_size_to_ard];
char * buffOut = new char[msg_size_to_pc];

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  Serial.setTimeout(1000);
  while (!Serial) {

    ;  // wait for serial port to connect. Needed for native USB port only
  }
  establishContact();  // send a byte to establish contact until receiver responds
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, LOW); // turn the LED on (HIGH is the voltage level)

}


void loop() {
  // put your main code here, to run repeatedly:
  if (Serial.available() >= msg_size_to_ard) {
    if (readSerial(data_in)) {
      data_out->displayed_speed = data_in->speed;
      data_out->displayed_rmp = data_in->rmp;
      data_out->buttonState = data_in->indicators;
      data_out->acceleratorValue = 5555; 
      data_out->brakeValue = 0; 
      data_out->clutchValue = 0;
      writeSerial(data_out);
    }
    // Cleanup leftowers
    if (Serial.available() > 0) {
      digitalWrite(LED_BUILTIN, HIGH); // turn the LED on (HIGH is the voltage level)
      while (Serial.available()>0) {
        Serial.read();
      }
    } else {
      digitalWrite(LED_BUILTIN, LOW); // turn the LED on (HIGH is the voltage level)
    }
  }
  delay(1);
 
  
}




bool readSerial(SDE_to_ARD* dataRecieved) {
  memset(buffIn, 0, msg_size_to_ard);
  if (Serial.readBytes(buffIn, msg_size_to_ard) ) {
    memcpy(data_in, buffIn, msg_size_to_ard);
    return true;
  }
  return false;
}



bool writeSerial(SDE_to_PC* dataToSend) {
  memset(buffOut, 0, msg_size_to_pc);
  memcpy(buffOut, data_out, msg_size_to_pc);
  Serial.write (buffOut, msg_size_to_pc);
}

void establishContact() {
  while ( Serial.read() != 'B') {
    Serial.print('A');  // send a capital A
    delay(300);
  }
  Serial.print('C');
  delay(10);
  while (Serial.available()>0) {
    Serial.read();
  }
  // assume we are good 
}

